#!/bin/bash
#Un script qui remplace récursivement un motif dans un répertoire ou un fichier.

FILE=${1:?"Vous devez fournir un chemin vers un fichier ou un dossier."}

if [ -e $FILE ]
then

        echo "Indiquez un motif à remplacer :"
        read PATTERN
        echo "Par quoi voulez-vous remplacer ce motif ?"
        read REPLACE

        if [ -d $FILE ]
        then
                cd $FILE
                find . -type f -print0 | xargs -0 sed -i "s|$PATTERN|$REPLACE|g"
        elif [ -f $FILE ]
        then
                sed -i "s|$PATTERN|$REPLACE|g" $FILE
        else
                echo "$FILE n'est ni un répertoire ni un fichier simple."
        fi

else
        echo "Le fichier ou le dossier indiqué n'existe pas."
fi